<!--
SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0
-->

Javascript module to cast teletext pages to Chromecast in supporting browsers.  (Supporting browsers are Chrome and possibly other browsers that support Goggle's [Chrome Sender SDK](https://developers.google.com/cast/docs/chrome_sender)).

# Usage

## As an ECMAScript module

The HTML/javascript code below does the following:
* Imports the Google cast API framework
* Shows a cast button
* Imports this module
* Uses it to cast a teletext page

```html
<script src="https://www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1"></script>
<google-cast-launcher id="castbutton"></google-cast-launcher>

<script type="module">
  import { ttxcaster } from 'https://cdn.jsdelivr.net/npm/@techandsoftware/teletext-caster@latest/dist/teletextcaster.min.js';

  // dispay a page using a base64url encoded string. See below about PACKED_PAGE
  ttxcaster.display(PACKED_PAGE)

  // display a page using Output Lines. Parameters described below
  ttxcaster.display({
    defaultG0Charset: encoding,
    header: header,
    outputLines: outputLines
  })
</script>
```

If using npm instead of jsdelivr, then install the package:

```bash
npm install @techandsoftware/teletext-caster
```

Use this import instead:

```javascript
import { ttxcaster } from './node_modules/@techandsoftware/teletext-caster/dist/teletextcaster.min.js';
```

## Non-module code or older browsers

If you're targetting browsers that don't support ES6 modules, or have an existing codebase that isn't based on ES modules, a UMD is available in the `dist` directory for importing using `<script>`. This exports the module in the global `ttx` so access it as `ttx.ttxcaster`

```html
<!-- from jsdelivr -->
<script src="https://cdn.jsdelivr.net/npm/@techandsoftware/teletext-caster@latest/dist/teletextcaster.umd.min.js"></script>

<!-- using npm -->
<script src="./node_modules/@techandsoftware/teletext-caster/dist/teletextcaster.umd.min.js"></script>
```

# API

The `ttxcaster.display` method is the only one which needs to be called to display a page. The other methods are optional.

(Google's API framework and cast button are discussed in [Google's guide](https://developers.google.com/cast/docs/chrome_sender/integrate). It's not necessary to call Google's API directly as this module wraps around it.)

## ttxcaster.display(PACKED_PAGE || object)

Displays a page. You can supply a string or an object.

PACKED_PAGE is a base64-encoded string of 7-bit characters for the 25 rows x 40 characters concatenated together. The encoded string uses the character repertoire defined in the [base64url encoding](https://tools.ietf.org/html/rfc4648#section-5). This format is taken from the URL hash fragment format used by Simon Rawles' online edit.tf teletext editor. See further details here: https://github.com/rawles/edit.tf

If an object is supplied, it has these properties:
```javascript
{
  defaultG0Charset: string (optional),
  header: string (optional),
  outputLines: [strings],
  packed: string
}
```

`defaultG0Charset` is optional.  If supplied, the page uses it as the default G0 character set.  The available character sets can be seen at [@techandsoftware/teletext](https://www.npmjs.com/package/@techandsoftware/teletext) in the documentation for `setDefaultG0Charset()`.

`header` is optional. When present, it's a string of 32 characters, which have the same encoding as the outputLines (see below) but without the initial `OL,rowNum,` . This is used as the header row and is used instead of Output Line 0 in the provided outputLines. When not provided, the row 0 in the lines is used if there is one.  When a header is supplied, the page is also cleared before the outputLines are displayed.

One of `outputLines` or `packed` is required, which are alternative formats for the page data.  If `packed` is supplied, it's a string with the PACKED_PAGE format described above.  `outputLines` is an array of strings with up to 25 elements. The strings are in the Output Line format used in MRG's .tti files.  Each line has this format:

`OL,rowNum,line`

In this:

* `rowNum` is between 0 and 24
* `line` is the string to display. Attribute characters (character codes less than 0x20) are represented in three ways: 1) As they are with no translation, or 2) They have 0x80 added to translate them to characters with codes 128-159, or 3) they are replaced by escape (character 0x1b) then the character with 0x40 added.

## ttxcaster.available.attach(function)

Register a function to receive the available event. The available event happens when the client browser (probably Chrome) supports Google's casting API, so there is the possibility of connecting to Chromecast devices and you can expect to get `castStateChanged` events. If the browser doesn't supporting casting then there's no event.

## ttxcaster.castStateChanged.attach(function)

Register a function for state change events. The function is called when the cast icon state changes. The state can be retrieved with `ttxcaster.getCastState()`.  (This is a proxy to [CAST_STATE_CHANGED](https://developers.google.com/cast/docs/reference/chrome/cast.framework#.CastContextEventType) events on [CastContext](https://developers.google.com/cast/docs/reference/chrome/cast.framework.CastContext) in Google's cast API.)

## ttxcaster.getCastState()

Gets the cast icon state. The response is a string with one of these values:

* `NO_DEVICES_AVAILABLE`
* `NOT_CONNECTED`
* `CONNECTING`
* `CONNECTED`

(This is a proxy to [CastContext.getCastState()](https://developers.google.com/cast/docs/reference/chrome/cast.framework.CastContext#getCastState) in Google's cast API.)

## ttxcaster.setSmoothMosaics()

Renders block mosaics using a pixel art scaling algorithm so that graphics are higher resolution.

## ttxcaster.setBlockMosaics()

Renders block mosaics in their usual blocky form.

## ttxcaster.clearScreen()

Clears the screen.

## ttxcaster.toggleReveal()

Shows or hides concealed characters if the page contains them.
    
## ttxcaster.toggleMixMode()

Hides or shows row background colours.

## ttxcaster.toggleBoxMode()

Activates or deactivates box mode mode display. The page needs to contain boxed characters for them to display, otherwise the page is transparent.
    
## ttxcaster.toggleGrid()

Shows or hides a grid.

# Demo

See a live demo in supporting browsers at https://teletextmoduledemo.tech-and-software.ltd.uk/teletext-caster-demo.html

# Licensing

The Chromecast receiver app that this module connects to is intended for non-commercial use.  For commercial enquiries, contact techandsoftwareltd@outlook.com

This package complies with [REUSE 3](https://reuse.software/).
