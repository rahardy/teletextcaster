// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0

import { terser } from "rollup-plugin-terser";
import nodeResolve from '@rollup/plugin-node-resolve';

const OUTPUT_BANNER = `// SPDX${''}-FileCopyrightText: (c) 2021 Tech and Software Ltd.
// SPDX${''}-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0
// LicenseRef-uk.ltd.TechAndSoftware-1.0 refers to https://tech-and-software.ltd.uk/LICENSES/LicenseRef-uk.ltd.TechAndSoftware-1.0.txt
//
// The Chromecast receiver app is for non-commerical use only. Contact techandsoftwareltd@outlook.com for commercial enquiries`;

export default {
  input: 'src/TeletextCaster.js',
  output: [
    {
      file: 'dist/teletextcaster.min.js',
      format: 'es',
      compact: true,
    },
    {
      file: 'dist/teletextcaster.umd.min.js',
      format: 'umd',
      name: 'ttx',
      compact: true,
    }
  ],
  plugins: [
    nodeResolve(),
    terser({
      ecma: 2017,
      toplevel: true,
      compress: {
        drop_console: true,
        passes: 2,
        pure_getters: true,
        unsafe_arrows: true,
      },
      mangle: {
        properties: {
          regex: /^_/,
        },
      },
      format: {
        preamble: OUTPUT_BANNER
      }
    }),
  ],
};
