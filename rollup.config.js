// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0

import nodeResolve from '@rollup/plugin-node-resolve';

export default {
  input: {
    teletextcaster: 'src/TeletextCaster.js',
  },
  output: {
    entryFileNames: '[name].js',
    dir: 'dist',
    format: 'es',
  },
  plugins: [
    nodeResolve(),
  ]
};
