// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0

/* globals cast, chrome */
import { Event } from './Event.js';

const CHROMECAST_APPLICATION_CUSTOM_MESSAGE_NAMESPACE = 'urn:x-cast:uk.ltd.techandsoftware.teletext';
// This is used to play valid media and prevent cast errors
const SILENCE_URL = 'https://teletextforchromecast.tech-and-software.ltd.uk/250-milliseconds-of-silence.mp3';

// The receiver app is for non-commerical use only. Contact techandsoftwareltd@outlook.com for commercial enquiries
const CHROMECAST_RECEIVER_APPLICATION_ID = '000F65B3';

class TeletextCaster {
    constructor() {
        this.available = new Event(this);
        this.castStateChanged = new Event(this);
    }

    _init() {
        if (typeof cast == 'undefined') {
            console.error("TeletextCaster: failed to init: 'cast' not defined");
            return;
        }
        const options = {
            receiverApplicationId: CHROMECAST_RECEIVER_APPLICATION_ID,
            autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED,
        };
        this._context = cast.framework.CastContext.getInstance();
        this._context.setOptions(options);
      
        this._remotePlayer = new cast.framework.RemotePlayer();
        this._remotePlayerController = new cast.framework.RemotePlayerController(this._remotePlayer);

        // following is for the remote app connected state, not needed currently
        // this._remotePlayerController.addEventListener(
        //     cast.framework.RemotePlayerEventType.IS_CONNECTED_CHANGED,
        //     e => this._handleChangeOfConnected(e.value)
        // );

        this._context.addEventListener(
            cast.framework.CastContextEventType.CAST_STATE_CHANGED,
            () => this.castStateChanged.notify()
        );
        // cast.framework.CastContext.getInstance().addEventListener(
        //     cast.framework.CastContextEventType.SESSION_STATE_CHANGED,
        //     e => console.log('sessionStageChanged', JSON.stringify(e))
        // );
        
        this.available.notify();
        this.castStateChanged.notify();
    }

    async display(packedPageOrObjectInput) {
        if (!this._isConnected()) return;

        const session = this._getSession();
        this._remotePlayerController.stop();
        const mediaInfo = new chrome.cast.media.MediaInfo(SILENCE_URL, 'audio/mpeg3');
        mediaInfo.entity = packedPageOrObjectInput;
        const request = new chrome.cast.media.LoadRequest(mediaInfo);
        // request.autoplay = true;
        try {
            await session.loadMedia(request);
            console.debug('TeletextCaster.display: loadMedia ok');
        } catch (e) {
            console.trace(e);
            console.dir(e);
            console.error('TeletextCaster.display failed:', e.toString());
        }
    }

    getCastState() {
        return this._context.getCastState();
    }

    clearScreen() {
        this._sendCommand('clear');
    }

    toggleGrid() {
        this._sendCommand('grid');
    }

    toggleReveal() {
        this._sendCommand('reveal');
    }

    toggleMixMode() {
        this._sendCommand('mix');
    }

    toggleBoxMode() {
        this._sendCommand('box');
    }

    setSmoothMosaics() {
        this._sendCommand('smoothmosaic');
    }

    setBlockMosaics() {
        this._sendCommand('blockmosaic');
    }

    async _sendCommand(command) {
        if (!this._isConnected()) return;

        const session = this._getSession();
        try {
            await session.sendMessage(CHROMECAST_APPLICATION_CUSTOM_MESSAGE_NAMESPACE, `"${command}"`);
            console.debug(`TeletextCaster._sendCommand: ${command} sent`)
        } catch (e) {
            console.error(`TeletextCaster._sendCommand E67: failed to send ${command}:`, e.toString());
        }
    }

    _isConnected() {
        return typeof this._remotePlayer == 'object' && this._remotePlayer.isConnected;
    }

    _getSession() {
        return this._context.getCurrentSession();
    }
}

export const ttxcaster = new TeletextCaster();

let checkForCastCount = 0;
window['__onGCastApiAvailable'] = isAvailable => {
    if (isAvailable) {
        if (typeof cast == 'undefined') {
            delayedCheckForCast();
        } else {
            ttxcaster._init();
        }
    }
};

// FUDGE cast isn't set reliably so we have to work around that
function delayedCheckForCast() {
    window.setTimeout(() => {
        if (typeof cast == 'undefined') {
            checkForCastCount++;
            if (checkForCastCount < 10) delayedCheckForCast();
        } else {
            ttxcaster._init();
        }
    }, 500);
}
